# DHBW Vorlesung: Daniel

- Folien grundlegend überarbeiten, moderneres Styling 
  - wir können hier reveal.js (slides.com) nutzen
- Vorgehens Folien bleibt ähnlich
  - Support für Eclipse und Intellij 
  - Vorstellungsfolien für uns
- Ein paar Folien zum Ausblick für die Vorlesung

## Vorbereitung

- Requirements vorher über die KurssprecherInnen kommunizieren
  - GIT, Maven, NPM
  - Links dafür raussuchen und bereitstellen -> TODO Kai

## 0. Einordnung des Themas

- Warum sind Webanwendungen so Zahlreich
  - Version von Client Anwendungen ist Aufwendig
    - Warum ist dies Aufwendig -> muss erklärt werden
    - Versioning tritt nicht in dem Maße im Web auf
  - Wartung verschiedener Plattformen
    - Web ist Plattformunabhängig
  - Datenübertragung übers Internet ist günstig
  - Einstiegshürde ist niedriger (HTML, JavaScript und CSS wird interpretiert nicht compiled)
    - JavaScript Entwickler gibt es sie Sand am Meer
- Trendige Technologien
  - 1 Slider mit einem aktuelleren Diagramm
  - https://www.youtube.com/watch?v=3xKxSNw26ZU
- Welche Themen bearbeiten wir
- Warum bearbeiten wir diese Themen
- Warum diese technologien und nicht andere

## 1. HTML Seiten dynamisch erzeugen: Daniel – Abschluss bis zum 09.08.2021

- JSF als Tooling
- Java eingebettet in HTML
- Todo Anwendung als Praxis zusammenbauen
- Grundkonzepte von JSF erklären
- Nicht auf Details und einzelne Tags eingehen
- Model, View, Controller anhand JSF erklären

## 2. Rich Client (Client) Anwendung: Kai

- Erklären wie heutzutage normale B2C Anwendungen gebaut werden
- Client Anwendungen mit JavaScript Framework, die nur noch Daten beim Backen abfragen und zurückschicken
  - Kein HTML wird mehr vom Backend hin und her geschickt
- Angular Konzepte vorstellen
  - Wir sollten hier möglichst basic bleiben
  - Module System, Components, Forms, Input Output Params, Services
- Frontend Architektur (Component System)
- Praxis: Beispiel Component vorschreiben
  - Die Studis können anschließend ihre Todo Anwendung vervollständigen
  - Wir versuchen bereits einen Malkasten vorzuliefern
  
## 3. Rich Client (Client) Anwendung: Kai

- Statemanagement Redux
- Statemanagement mit Angular (NgRx)
- Statemanagement in anderen Frameworks React, Vue, etc.
- Praxis: Einbau eines Statemanagement in die Angular Anwendung.

## 4. Rich Client (Server) Anwendung: Iven

- Architektur eines Backends
  - Monolith
  - Service Architekturen
  - Wann verwende ich was?
- Vergleich zu JavaEE??? oder andere Backend Frameworks
- Praxis: Bau eines Webservers mit Spring
- Tests für Postman die den Code der Studis abtesten
- Bauen einer Todo Anwendung

## 5. Benutzerverwaltung: Kai + Daniel

- Grundlegende Technik zur Benutzerverwaltung
- Theorie zur Benutzerverwaltung
- JWT sollten wir besprechen
- Anhand der vorher geschriebenen Angular Anwendung
- Wir bereiten Backendseitig soweit alles für die Benutzerverwaltung vor
- Frontendseitig können wir es selbst programmieren
- Feature einbauen das mit der Client ID gebaut wird

## 6. Security: Daniel

- Theorie zu Security
  - XSS 
  - SQL Injection 
  - etc.
- Beispiele zu jeder Einbruchsmethode
- Wie kann man sich davor schützen
- Angriff auf die selbstgeschriebene Anwendung
- Security einbauen, z.b. PreparedStatements

## 7. Apps: (Bonusthema)

- Nativ vs Hybride Webapp vs Plattformübergreifend
- Apps mit Flutter
- Vergleich zu anderen technologien
- Praxis: Todo App in Flutter







