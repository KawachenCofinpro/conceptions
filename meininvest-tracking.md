# Meininvest Tracking in der Zukunft

Wir schalten das Meininvest Tracking erstmal ab, aber was ist die Alternative? Wie geht es mit dem Meininvest Tracking in Zukunft weiter?

Da wir im Rahmen des Consent Management Themas sowieso einen größeren Umbau am Meininvest Tracking vornehmen müssten, wollen wir das Tracking an sich ändern, um es zu verbessern. 

## Warum wollen wir das Meininvest Tracking ändern?

### Argumente für Extern

- Meininvest Tracking weicht von der Visualvest ab
  - damit haben wir doppelten Wartungsaufwand 
  - Meininvest Banken profitieren nicht von den Erfahrungen der Visualvest
- keine Dokumentation zu den fachlichen Events
- Banken betreiben hauptsächlich Kampagnentracking
    - damit sind die meisten Events sehr wahrscheinlich völlig uninteressant
- Datenschutzprobleme z.B. mit Google Analytics

### Argumente für Intern

- keine Qualitätssicherung
- Features funktionieren nicht korrekt
- veraltete und ungetestete Software/Skripte zum Aktivieren des Trackings
- die meisten Trackingdaten helfen den Banken wenig, da sie keinen direkten Einfluss auf die Features der Plattform haben
    - wenn wir die Daten der Banken auswerten würde, würde uns das mehr helfen
    
## Wie soll das neue Meininvest Tracking aussehen

### Meininvest Tracking soll keine Sonderlösung mehr sein
- Änderungen:
    - das gesamte Tracking wird auf Matomo umgestellt
    - Matomo Instanzen werden von uns gehostet (Datenschutz)
    - funktional verhält sich das Tracking analog zum Tracking bei der Visualvest
- Vorteile:
    - Komplexität und Entwicklungs-/Wartungsaufwand wird gesenkt
    - Qualitätssicherung wird einfacher
    - Datenschutz
    - Dokumentation kann übergreifend genutzt werden
    - Auswertungen können übergreifend genutzt werden
### Aktivierung des Meininvest Trackings wird neu aufgesetzt
- Änderungen:
    - es soll nur noch eine Checkbox zum Aktivieren/Deaktivieren geben
    - veraltete Aktivierungsskripte werden entfernt oder gegebenenfalls ersetzt
    - möglicherweise brauchen wir sogar keine Skripte mehr zur Aktivierung
- Vorteile:
    - Aktivierungsprozess wird einfacher und damit weniger anfällig für Fehler
    - veralteter ungetesteter Code wird entfernt oder ersetzt
### Auswertungen für die Banken
- Änderungen:
    - Kampagnentracking soll weiterhin für die Banken selbst möglich sein
        - Hierfür ist für die Banken wahrscheinlich nur die Kampagne und der Lead und Sale interessant
    - (Optional) Auswertungen von der VV werden auf die Daten der Banken übertragen
        - die Banken können in regelmäßigen Abständen diese Auswertungen erhalten
- Vorteile:
    - Kampagnentracking können die Banken weiterhin selbst übernehmen
    - Wir profitieren von den Erfahrungen und vorarbeiten von den Analytics Experten bei der VV
    
    


